package com.makaryk.People;

public class Woman {
    private String name;

    public Woman(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Woman{" +
                "name='" + name + '\'' +
                '}';
    }
    public void generateMessage() {
        System.out.println("I am woman: ");
    }
}
