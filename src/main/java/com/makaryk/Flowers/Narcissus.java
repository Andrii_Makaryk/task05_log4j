package com.makaryk.Flowers;

public class Narcissus {
    private String name;

    public Narcissus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Narcissus{" +
                "name='" + name + '\'' +
                '}';
    }
    public void generateMessage() {
        System.out.println("I am narcissus...");
    }
}
