package com.makaryk.Flowers;

public class Rose {
    private String name;

    public Rose(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Rose{" +
                "name='" + name + '\'' +
                '}';
    }

    public void generateMessage() {
        System.out.println("I am rose...");
    }
}
