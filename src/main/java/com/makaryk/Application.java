package com.makaryk;

import com.makaryk.Animals.Cat;
import com.makaryk.Animals.Dog;
import com.makaryk.Flowers.Narcissus;
import com.makaryk.Flowers.Rose;
import com.makaryk.People.Man;
import com.makaryk.People.Woman;
import org.apache.logging.log4j.*;

import java.io.IOException;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        //send log to mail
        Rose rose = new Rose("Olia");
        rose.generateMessage();
        //Send log to sms
        Narcissus narcissus = new Narcissus("Olenka");
        narcissus.generateMessage();

        logger1.error("Exception", new IOException("Hi"));

        System.out.println("Hello");
        logger1.trace("This is trace message");
        logger1.debug("This is trace message");
        logger1.info("This is info message");
        logger1.warn("This is warn message");
        logger1.error("This is error message");
        logger1.fatal("This is fatal message");

        Man man=new Man("Andrii");
        Woman woman=new Woman("Galina");
        Dog dog=new Dog("Barbos");
        Cat cat=new Cat("Murchik");
        for (int i=0;i<5;i++){
            man.generateMessage();
            woman.generateMessage();
            cat.generateMessage();
            dog.generateMessage();
        }

/*
 RollingFile:
    #every day log will be written archive file
    name: rolFile3
    fileName: ${log-path}/rollingFile3.log
    filePattern: ${log-path}/archive/rolFile3-%d{yyyy-MM-dd}.log
    PatternLayout:
      Pattern: "[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} %class{1}:%L -%msg
                    %xEx{short}%n"
    Policies:
      TimeBasedTriggeringPolicy:
       interval: 1
       modulate: true
  SMTP:
    name: MailAppender
    subject: "Error Log"
    to: "makaryk_a_wat2006@bigmir.net"
    from: "makarikandrii@gmail.com"
    smtpHost: "smtp.gmail.com"
    smtpPort: 465
    smtpProtocol: smtps
    smtpUsername: "makaryk_a_wat2006@bigmir.net"
    smtpPassword: "09121983Andrii"
    bufferSize: 512
    HTMLLayout:
      charset: "UTF-8"
      title: "Logs from Makaryk project 5_Log4"
      Logger:
      -name: com.makaryk.Flowers.Narcissus
       level: all
       AppenderRef:
         -ref: AsyncSMS
         level: warn
     Root: all
      level: all
      AppenderRef:
      -ref: rootLogFile
        level: debug

      Logger:
       - name: com.makaryk.Application
         level: all
         AppenderRef:
           -ref: logFile1
            level: info
            -ref: Console_Appender
             level: all

       -name: com.makaryk.Animals.Dog
         level: all
           AppenderRef:
             -ref: rolFile3
             level: debug
              -ref: rolFile4
              level: debug
               RollingFile:
        #every day log will be written archive file
        name: rolFile3
        fileName: ${log-path}/rollingFile3.log
        filePattern: ${log-path}/archive/rolFile3-%d{yyyy-MM-dd}.log
        PatternLayout:
          Pattern: "[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} %class{1}:%L -%msg
                          %xEx{short}%n"
        Policies:
          TimeBasedTriggeringPolicy:
            interval: 1
            modulate: true

 */
    }
}
