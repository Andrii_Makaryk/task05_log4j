package com.makaryk.SMS;

import java.io.Serializable;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

//note:class name need not much the @Plugin name
@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public class SmsAppender extends AbstractAppender {
    protected SmsAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override   //here I send SMS-message
    public void append(LogEvent logEvent) {

    }
}
